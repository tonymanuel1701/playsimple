export const searchMenus = (menus, searchString) => {
  let results = [];
  menus.filter((item) => {
    if (item.title.toLowerCase() === searchString.toLowerCase()) {
      results.push(item);
    } else if (item.subMenu) {
      item.subMenu.filter((sub) => {
        if (sub.title.toLowerCase() == searchString.toLowerCase()) {
          results.push(sub);
        }
      });
    }
  });
  return results;
};
