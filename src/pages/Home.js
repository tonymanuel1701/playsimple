import BannerActions from "../components/PageUtils/BannerAction";
import BannerTitle from "../components/PageUtils/BannerTitle";
import ImageBanner from "../components/PageUtils/ImageBanner";
import Wrapper from "../components/PageUtils/Wrapper";
import { HOME_BANNER } from "../constants/cdnLinks";

const Home = ({
  title = "SUPERCELL",
  subTitle = "Makers of Hay Day, Clash of Clans, Boom Beach, Clash Royale and Brawl Stars.",
  scrollTo = "#",
}) => (
  <Wrapper>
    <ImageBanner imageSrc={HOME_BANNER} />
    <BannerTitle title={title} subTitle={subTitle} />

    {/* <Wrapper className={classes.gridWrapper} id={id}>
        {FEATURED_GAMES.map((game) => (
          <FeaturedGames {...game} />
        ))}
      </Wrapper> */}
  </Wrapper>
);
export default Home;
