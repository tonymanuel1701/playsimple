import { useEffect } from "react";
import ImageBanner from "../components/PageUtils/ImageBanner";
import Panel from "../components/PageUtils/Panel/Panel";
import Wrapper from "../components/PageUtils/Wrapper";
import {
  CDN_HAY_DAY_COMMUNITY_BANNER,
  CDN_HAY_DAY_INTRO,
  CDN_IMAGE_HAY_DAY_BANNER,
  CDN_NEWS_INTRO,
} from "../constants/cdnLinks";
import { useDispatch, useSelector } from "react-redux";
import { getEvolving, getFarmWithFamily } from "../store/HayDay/actionCreators";
import { URL_PARAGH_1, URL_PARAGH_2 } from "../constants/apiEndPoints";
import AppStores from "../components/AppStore/AppStores";
import { AMAZON, GOOGLE, IOS } from "../json/storeRoutes";
import SocialNetworks from "../components/SocialNetworks/SocialNetworks";
import { FACEBOOK, INSTAGRAM, TWITTER, YOUTUBE } from "../json/socialRoutes";
import { FARM_WITH_FRIENDS, HAYDAY_COMMUNITY } from "../json/dummyTexts";
import TimeLine from "../components/games/TimeLine";
import Medias from "../components/games/Medias";

const FarmWithFamily = () => (
  <Panel
    title={FARM_WITH_FRIENDS.title}
    footer={
      <AppStores
        stores={[IOS, GOOGLE, AMAZON]}
        style={{ justifyContent: "flex-start" }}
      />
    }
    body={FARM_WITH_FRIENDS.body}
    image={CDN_HAY_DAY_INTRO}
  />
);

const Evolving = () => (
  <Panel
    banner={CDN_HAY_DAY_COMMUNITY_BANNER}
    title={HAYDAY_COMMUNITY.title}
    footer={
      <SocialNetworks networks={[YOUTUBE, FACEBOOK, INSTAGRAM, TWITTER]} />
    }
    body={HAYDAY_COMMUNITY.body}
    image={CDN_NEWS_INTRO}
    fontColor="#fff"
  />
);

const HayDay = () => {
  const dispatch = useDispatch();

  const { evolving, farmWithFamily } = useSelector(
    (state) => state.hayDayReducer
  );

  useEffect(() => {
    dispatch(getEvolving(URL_PARAGH_1));
    dispatch(getFarmWithFamily(URL_PARAGH_2));
  }, []);

  return (
    <Wrapper>
      <ImageBanner imageSrc={CDN_IMAGE_HAY_DAY_BANNER} />
      <FarmWithFamily />
      <TimeLine />
      <Evolving />
      <Medias />
    </Wrapper>
  );
};
export default HayDay;
