import FeaturedGames from "../components/games/FeaturedGames";
import BackGroundDiv from "../components/PageUtils/BackGroundDiv";
import BannerActions from "../components/PageUtils/BannerAction";
import BannerTitle from "../components/PageUtils/BannerTitle";
import ImageBanner from "../components/PageUtils/ImageBanner";
import Wrapper from "../components/PageUtils/Wrapper";
import { CDN_IMAGE_GAMES_BANNER } from "../constants/cdnLinks";
import FEATURED_GAMES from "../json/featuredGames";
import gameStyles from "../styles/games-styles";

const Games = ({
  id = "feature",
  scrollTo = "#feature",
  title = "Games",
  subTitle = "Farm, Clash, Boom, Battle, Brawl!",
}) => {
  const classes = gameStyles();
  return (
    <Wrapper>
      <ImageBanner imageSrc={CDN_IMAGE_GAMES_BANNER} />
      <BannerTitle title={title} subTitle={subTitle} />
      <BannerActions scrollTo={scrollTo} />

      <Wrapper className={classes.gridWrapper} id={id}>
        {FEATURED_GAMES.map((game) => (
          <FeaturedGames {...game} />
        ))}
      </Wrapper>
    </Wrapper>
  );
};
export default Games;
