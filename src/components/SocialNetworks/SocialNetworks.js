import {
  FACEBOOK,
  GLASSDOOR,
  INSTAGRAM,
  LINKEDIN,
  TWITTER,
  YOUTUBE,
} from "../../json/socialRoutes";
import BackGroundDiv from "../PageUtils/BackGroundDiv";
import Wrapper from "../PageUtils/Wrapper";
import pageUtilsStyles from "../../styles/page-utils";
import { Link } from "react-router-dom";

const SocialNetworks = ({
  networks = [YOUTUBE, FACEBOOK, INSTAGRAM, TWITTER, LINKEDIN, GLASSDOOR],
  target = "_blank",
}) => {
  const classes = pageUtilsStyles();
  return (
    <Wrapper className={classes.socialIconsWrapper}>
      {networks.map(({ icon, path }) => (
        <Link
          to={{ pathname: path }}
          target={target}
          className={classes.socialIcons}
        >
          <BackGroundDiv imageSrc={icon} />
        </Link>
      ))}
    </Wrapper>
  );
};
export default SocialNetworks;
