import { GOOGLE, IOS } from "../../json/storeRoutes";
import AppStores from "../AppStore/AppStores";
import Wrapper from "../PageUtils/Wrapper";
import SocialNetworks from "../SocialNetworks/SocialNetworks";
import pageUtilsStyles from "../../styles/page-utils";
import SecondaryMenus from "../navbar/SecondaryMenus";
import Brand from "../navbar/Brand";

const FollowUs = ({ title, children, className }) => (
  <Wrapper>
    <h5 className={className}>{title}</h5>
    {children}
  </Wrapper>
);
const Divider = ({ className }) => <Wrapper className={className} />;

const Address = ({ className }) => (
  <Wrapper className={className}>
    <span>Supercell Oy</span>
    <span>Jätkäsaarenlaituri 1</span>
    <span>00180 Helsinki</span>
    <span>Finland</span>
  </Wrapper>
);

const Footer = ({ followUsTitle = "Follow Us On", children }) => {
  const classes = pageUtilsStyles();
  return (
    <Wrapper className={classes.footer}>
      <Wrapper className={classes.gridContainer}>
        <FollowUs title={followUsTitle} className={classes.mediumText}>
          <SocialNetworks />
        </FollowUs>
        <AppStores stores={[GOOGLE, IOS]} />
      </Wrapper>
      <Divider className={classes.dividerH} />
      <SecondaryMenus />
      <Wrapper className={classes.addressWrapper}>
        <Address className={[classes.address, classes.mediumText]} />
        <Brand />
      </Wrapper>
    </Wrapper>
  );
};

export default Footer;
