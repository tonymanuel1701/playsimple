/**
 * Use this component to add a background image to a div
 */

import pageUtilsStyles from "../../styles/page-utils";
const BackGroundDiv = ({
  imageSrc = "",
  className = [],
  children,
  style = {},
}) => {
  const classes = pageUtilsStyles();

  //Converting the array of class names recieved through props to string
  className = Array.isArray(className) ? className.join(" ") : className;
  className = [className, classes.bgImage].join(" ");

  return (
    <div
      style={{
        backgroundImage: `url(${imageSrc})`,
        ...style,
      }}
      className={className}
    >
      {children}
    </div>
  );
};
export default BackGroundDiv;
