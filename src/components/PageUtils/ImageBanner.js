/**
 * Use this component to create a  main image banner.
 * Refer https://supercell.com/en/
 */
import bannerStyles from "../../styles/image-banner-styles";
import Wrapper from "./Wrapper";

const ImageBanner = ({ imageSrc, style }) => {
  const classes = bannerStyles();

  return (
    <Wrapper style={style} className={classes.wrapper}>
      <img src={imageSrc} />
    </Wrapper>
  );
};
export default ImageBanner;
