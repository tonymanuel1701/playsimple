import useStyles from "../../styles/page-utils";
import Wrapper from "./Wrapper";

const Title = ({ children }) => <h2>{children}</h2>;
const SubTitle = ({ children }) => <p>{children}</p>;

const BannerTitle = ({ title, subTitle }) => {
  const classes = useStyles();
  return (
    <Wrapper className={classes.bannerTitle}>
      <Title>{title}</Title>
      <SubTitle>{subTitle}</SubTitle>
    </Wrapper>
  );
};
export default BannerTitle;
