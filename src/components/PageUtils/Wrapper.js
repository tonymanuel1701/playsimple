/**
 * This componet wraps the children within a div and returns
 */
const Wrapper = ({
  children = "",
  className = [],
  style = {},
  id = "",
  handler = () => {},
}) => {
  className = Array.isArray(className) ? className.join(" ") : className;

  return (
    <div style={{ ...style }} className={className} id={id} onClick={handler}>
      {children}
    </div>
  );
};
export default Wrapper;
