/**
 * Use this component to render a button in a image banner.
 * Refer https://supercell.com/en/
 */
import { CDN_CHEVRON_DOWN } from "../../constants/cdnLinks";
import useStyles from "../../styles/page-utils";
import BackGroundDiv from "./BackGroundDiv";
import Wrapper from "./Wrapper";
import AnchorLink from "react-anchor-link-smooth-scroll";

const BannerActions = ({ scrollTo = "#", actionTitle = "SEE LATEST" }) => {
  const classes = useStyles();
  return (
    <Wrapper className={classes.bannerAction}>
      <AnchorLink href={scrollTo}>
        <span className={classes.chevron}>
          <img src={CDN_CHEVRON_DOWN} />
        </span>
        <span className={classes.actionLabel}>{actionTitle}</span>
      </AnchorLink>
    </Wrapper>
  );
};
export default BannerActions;
