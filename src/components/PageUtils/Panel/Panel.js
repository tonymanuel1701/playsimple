import useStyles from "../../../styles/page-utils";
import BackGroundDiv from "../BackGroundDiv";
import Wrapper from "../Wrapper";
import PanelBody from "./PanelBody";
import PanelFooter from "./PanelFooter";
import PanelTitle from "./PanelTitle";

const Panel = ({
  banner = "",
  body,
  title,
  image,
  footer,
  fontColor = "#000",
}) => {
  const classes = useStyles();
  return (
    <div style={{ height: "600px" }}>
      <BackGroundDiv
        imageSrc={banner}
        style={{ backgroundPosition: "unset", backgroundSize: "cover" }}
      >
        <Wrapper className={classes.gridContainer} style={{ padding: "5%" }}>
          <div>
            <img src={image} className={classes.panelIntroImage} />
          </div>
          <div className={classes.panel} style={{ color: fontColor }}>
            <PanelTitle>{title}</PanelTitle>
            <PanelBody>{body}</PanelBody>
            <PanelFooter>{footer}</PanelFooter>
          </div>
        </Wrapper>
      </BackGroundDiv>
    </div>
  );
};
export default Panel;
