import useStyles from "../../../styles/page-utils";
import Wrapper from "../Wrapper";

const PanelFooter = ({ children }) => {
  const classes = useStyles();
  return <Wrapper>{children}</Wrapper>;
};

export default PanelFooter;
