import useStyles from "../../../styles/page-utils";

const PanelBody = ({ children }) => {
  const classes = useStyles();

  return <p className={classes.panelBody}>{children}</p>;
};

export default PanelBody;
