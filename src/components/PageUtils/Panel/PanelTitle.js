import useStyles from "../../../styles/page-utils";

const PanelTitle = ({ className, children }) => {
  const classes = useStyles();
  return <h2 className={classes.panelTitle}>{children}</h2>;
};

export default PanelTitle;
