import { NavLink } from "react-router-dom";
import Wrapper from "../PageUtils/Wrapper";

const SubMenu = ({ subMenus = [], className, isShow }) => (
  <Wrapper
    style={{
      position: "absolute",
      margin: "-15px",
      width: "100%",
      zIndex: -1,
      transform: isShow ? "translateY(78px)" : "translateY(0)",
      transition: "all 0.2s",
    }}
  >
    <Wrapper className={className.subMenu}>
      {subMenus.map(({ title, route, target }) => (
        <NavLink
          className={className.link}
          to={{ pathname: route }}
          target={target}
        >
          {title}
        </NavLink>
      ))}
    </Wrapper>
  </Wrapper>
);

export default SubMenu;
