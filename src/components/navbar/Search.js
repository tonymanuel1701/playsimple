import useStyles from "../../styles/navbar-styles";
import Wrapper from "../PageUtils/Wrapper";

const Search = ({ handler }) => {
  const classes = useStyles();
  return (
    <Wrapper style={{ margin: "0px 15px 0px 25px" }}>
      <span className={classes.searchIcon} onClick={handler} />
    </Wrapper>
  );
};

export default Search;
