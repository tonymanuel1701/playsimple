import { Link } from "react-router-dom";
import { CDN_SUPERCELL_ID } from "../../constants/cdnLinks";
import { PATH_SUPERCELL_ID } from "../../constants/routes";
import useStyles from "../../styles/navbar-styles";
import BackGroundDiv from "../PageUtils/BackGroundDiv";

const SuperCell = ({ imageSrc = CDN_SUPERCELL_ID }) => {
  const classes = useStyles();
  return (
    <Link
      to={{ pathname: PATH_SUPERCELL_ID }}
      target="_blank"
      className={classes.superCell}
    >
      <BackGroundDiv imageSrc={imageSrc} />
    </Link>
  );
};
export default SuperCell;
