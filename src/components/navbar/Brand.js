import { Link } from "react-router-dom";
import { CDN_LOGO } from "../../constants/cdnLinks";
import BackGroundDiv from "../PageUtils/BackGroundDiv";
import { PATH_HOME } from "../../constants/routes";
import pageUtilsStyles from "../../styles/page-utils";

const Brand = ({ path = PATH_HOME, imageSrc = CDN_LOGO }) => {
  const classes = pageUtilsStyles();
  return (
    <Link to={path}>
      <BackGroundDiv imageSrc={imageSrc} className={classes.brand} />
    </Link>
  );
};

export default Brand;
