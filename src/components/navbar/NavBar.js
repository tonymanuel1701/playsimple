import { useState, useRef } from "react";
import Wrapper from "../PageUtils/Wrapper";
import NAV_LINKS from "../../json/navMenus";
import Brand from "./Brand";
import useStyles from "../../styles/navbar-styles";
import SuperCell from "./SuperCell";
import Search from "./Search";
import NavLinks from "./NavLinks";
import SubMenu from "./SubMenu";
import SearchResults from "./SearchResults";
import SearchInput from "./SearchInput";
import { searchMenus } from "../../helper";

const SuperCellWrapper = ({ children, className, style = {} }) => (
  <Wrapper style={style} className={className}>
    {children}
  </Wrapper>
);

const NavBar = () => {
  const [subMenus, setSubMenu] = useState([]);
  const [showMenu, setShowMenu] = useState(false);
  const [toggleSearch, setToggleSearch] = useState(false);
  const classes = useStyles();
  const [searchResults, setSearchResults] = useState([]);
  const [showResults, setShowResults] = useState(false);
  const inputRef = useRef();

  const searchToggleHandler = () => {
    setToggleSearch(!toggleSearch);
  };

  const searchHandler = (e) => {
    if (e.keyCode === 13) {
      let results = searchMenus(NAV_LINKS, e.target.value);
      setSearchResults(results);
      setShowResults(true);
    }
  };

  const closeResultsHandler = () => {
    setShowResults(false);
    setSearchResults([]);
    setToggleSearch(!toggleSearch);
    inputRef.current.value = "";
  };
  return (
    <>
      <Wrapper className={classes.wrapper}>
        <Brand />
        <Wrapper className={classes.innerWrapper}>
          <NavLinks links={NAV_LINKS} className={classes.link} />

          <SearchInput
            style={{
              transform: toggleSearch ? "translateY(0)" : "translateY(-100vh)",
              opacity: toggleSearch ? "1" : "0",
            }}
            className={classes.searchInput}
            searchHandler={searchHandler}
            searchRef={inputRef}
          />

          <SuperCellWrapper
            style={{
              opacity: !toggleSearch ? "1" : "0",
              visibility: !toggleSearch ? "visible" : "hidden",
            }}
            className={classes.superCellWrapper}
          >
            <SuperCell />
          </SuperCellWrapper>
        </Wrapper>

        <Search handler={searchToggleHandler} />

        <SubMenu
          isShow={showMenu}
          subMenus={subMenus}
          className={{ subMenu: classes.subMenu, link: classes.link }}
        />
      </Wrapper>
      <SearchResults
        results={searchResults}
        showResults={showResults}
        closeResultsHandler={closeResultsHandler}
      />
    </>
  );
};
export default NavBar;
