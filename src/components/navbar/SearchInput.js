import Wrapper from "../PageUtils/Wrapper";

const SearchInput = ({
  style,
  className,
  inputType = "text",
  placeHolder = "Search",
  searchHandler,
  searchRef,
}) => (
  <Wrapper style={style} className={className}>
    <input
      type={inputType}
      placeholder={placeHolder}
      onKeyDown={searchHandler}
      ref={searchRef}
    />
  </Wrapper>
);
export default SearchInput;
