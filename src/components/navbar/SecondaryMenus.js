/**
 * This commponnent is used to render menus on  footer
 */

import { NavLink } from "react-router-dom";
import SECONDARY_MENUS from "../../json/secondaryMenus";
import Wrapper from "../PageUtils/Wrapper";
import pageUtilsStyles from "../../styles/page-utils";

const SecondaryMenus = ({ secondaryMenus = [...SECONDARY_MENUS] }) => {
  const classes = pageUtilsStyles();
  return (
    <Wrapper className={classes.secondaryMenus}>
      {secondaryMenus.map(({ title, route, target }) => (
        <NavLink
          to={{ pathname: route }}
          target={target}
          className={classes.mediumText}
        >
          {title}
        </NavLink>
      ))}
    </Wrapper>
  );
};

export default SecondaryMenus;
