import { NavLink } from "react-router-dom";
import Wrapper from "../PageUtils/Wrapper";

const NavLinks = ({ links = [], className }) => (
  <Wrapper>
    {links.map(({ title, route, subMenu = [], target }) => (
      <div class="subnav">
        <span>
          <NavLink
            className={className}
            to={{ pathname: route }}
            target={target}
          >
            {title}
          </NavLink>
          {subMenu.length > 0 && (
            <span class="subnav-content">
              {subMenu.map(({ title, route, subMenu = [], target }) => (
                <NavLink
                  className={className}
                  to={{ pathname: route }}
                  target={target}
                >
                  {title}
                </NavLink>
              ))}
            </span>
          )}
        </span>
      </div>
    ))}
  </Wrapper>
);
export default NavLinks;
