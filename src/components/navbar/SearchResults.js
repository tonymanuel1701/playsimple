import useStyles from "../../styles/navbar-styles";
import Wrapper from "../PageUtils/Wrapper";
const TableHeader = ({ page = "PAGE 1/13" }) => (
  <Wrapper
    style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
    }}
  >
    <h4>SEARCH RESULTS</h4>
    <h4>{page}</h4>
  </Wrapper>
);

const TableContent = ({ className, title, content }) => (
  <Wrapper className={className}>
    <h3>{title}</h3>
    <p>{content}</p>
  </Wrapper>
);
const SearchResults = ({ results = [], showResults, closeResultsHandler }) => {
  const classes = useStyles();

  return (
    <Wrapper
      className={classes.searchResultsWrapper}
      style={{
        transform: showResults ? "translateY(0)" : "translateY(-100vh)",
        transition: "all 0.5s ease",
      }}
    >
      <div
        style={{ fontSize: "32px", textAlign: "right", cursor: "pointer" }}
        onClick={closeResultsHandler}
      >
        x
      </div>
      <TableHeader />
      {results.map(({ title, route }) => (
        <TableContent
          className={classes.searchResultsContent}
          title={title}
          content={route}
        />
      ))}
    </Wrapper>
  );
};
export default SearchResults;
