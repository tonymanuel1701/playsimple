import { CDN_YOUTUBE_ICON_FULL, CDN_PLAY_ICON } from "../../constants/cdnLinks";
import useStyles from "../../styles/video-thumbnails-styles";
import BackGroundDiv from "../PageUtils/BackGroundDiv";
import Wrapper from "../PageUtils/Wrapper";

const Thumbnail = ({ image, className }) => (
  <Wrapper className={className.thumbnailVideo}>
    <BackGroundDiv imageSrc={image}>
      <BackGroundDiv
        imageSrc={CDN_PLAY_ICON}
        className={className.thumbnailPlay}
      />
    </BackGroundDiv>
  </Wrapper>
);
const ThumbnailFooter = ({ title, icon, className }) => (
  <Wrapper className={className}>
    <h4>{title}</h4>
    <BackGroundDiv imageSrc={icon} />
  </Wrapper>
);

const VideoTumbnail = ({ title, icon, thumbnail, startVideo, path }) => {
  const classes = useStyles();

  return (
    <Wrapper
      className={classes.thumbnailWrapper}
      handler={() => startVideo(path)}
    >
      <Thumbnail
        image={thumbnail}
        className={{
          thumbnailVideo: classes.thumbnailVideo,
          thumbnailPlay: classes.thumbnailPlay,
        }}
      />
      <ThumbnailFooter
        title={title}
        icon={CDN_YOUTUBE_ICON_FULL}
        className={classes.thumbnailFooter}
      />
    </Wrapper>
  );
};

export default VideoTumbnail;
