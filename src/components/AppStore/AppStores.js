import { AMAZON, IOS, GOOGLE } from "../../json/storeRoutes";
import BackGroundDiv from "../PageUtils/BackGroundDiv";
import Wrapper from "../PageUtils/Wrapper";
import pageUtilsStyles from "../../styles/page-utils";
import { Link } from "react-router-dom";

const AppStores = (
  { stores = [AMAZON, IOS, GOOGLE], style },
  target = "_blank"
) => {
  const classes = pageUtilsStyles();
  return (
    <Wrapper style={style} className={classes.storeLinksWrapper}>
      {stores.map(({ icon, path }) => (
        <Link to={{ pathname: path }} target={target}>
          <BackGroundDiv imageSrc={icon} className={classes.storeLinks} />
        </Link>
      ))}
    </Wrapper>
  );
};
export default AppStores;
