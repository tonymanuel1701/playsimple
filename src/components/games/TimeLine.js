import { CDN_BG_TIMELINE_HAYDAY } from "../../constants/cdnLinks";
import { EVOLVING } from "../../json/dummyTexts";
import timeLine from "../../json/timeLine";
import useStyles from "../../styles/page-utils";
import PanelBody from "../PageUtils/Panel/PanelBody";
import PanelTitle from "../PageUtils/Panel/PanelTitle";
import Wrapper from "../PageUtils/Wrapper";

const TimeLine = () => {
  const classes = useStyles();
  return (
    <Wrapper style={{ padding: "5%", height: "500px" }}>
      <Wrapper style={{ display: "grid", gridTemplateColumns: "40% " }}>
        <PanelTitle>{EVOLVING.title}</PanelTitle>
        <PanelBody>{EVOLVING.body}</PanelBody>
      </Wrapper>
      <Wrapper
        style={{
          display: "flex",
          position: "relative",
          justifyContent: "space-between",
        }}
      >
        <Wrapper>
          {timeLine.map((mileStone) => (
            <Wrapper className={classes.mileStones}>{mileStone}</Wrapper>
          ))}
        </Wrapper>

        <Wrapper className={classes.hayDay}>
          <img src={CDN_BG_TIMELINE_HAYDAY} />
        </Wrapper>
        <Wrapper className={classes.scrollContainer}></Wrapper>
      </Wrapper>
    </Wrapper>
  );
};
export default TimeLine;
