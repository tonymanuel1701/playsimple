import { Link } from "react-router-dom";
import gamesStyles from "../../styles/games-styles";
import Wrapper from "../PageUtils/Wrapper";

const Title = ({ pathName, title, className, target }) => (
  <Link to={{ pathname: pathName }} className={className} target={target}>
    {title}
  </Link>
);
const SubTitle = ({ subTitle, className }) => (
  <p className={className}>{subTitle}</p>
);

const Image = ({ pathName, className, imageSrc, target }) => (
  <Link to={{ pathname: pathName }} className={className} target={target}>
    <img src={imageSrc} style={{ height: "auto", width: "100%" }} />
  </Link>
);
const FeaturedGames = ({ imageSrc = "", title, subTitle, route, target }) => {
  const classes = gamesStyles();
  return (
    <Wrapper className={classes.featureWrapper}>
      <Image pathName={route} imageSrc={imageSrc} target={target} />
      <SubTitle subTitle={subTitle} className={classes.subTitle} />
      <Title
        title={title}
        pathName={route}
        className={classes.title}
        target={target}
      />
    </Wrapper>
  );
};

export default FeaturedGames;
