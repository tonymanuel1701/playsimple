import { useState } from "react";

import YouTube from "react-youtube";
import { CAKE, WOOL } from "../../json/videoRouter";
import VideoTumbnail from "../media-player/VideoThumnail";
import Wrapper from "../PageUtils/Wrapper";

const opts = {
  playerVars: {
    autoplay: 1,
    mute: 1,
  },
};
const Medias = () => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [videoId, setVideoId] = useState("");
  const startVideo = (videoId) => {
    setVideoId(videoId);
    setIsPlaying(true);
  };

  const stopVideo = () => {
    setIsPlaying(false);
  };

  return (
    <Wrapper
      style={{
        background: "#f2f2f2",
        display: "flex",
        flexWrap: "no-wrap",
        justifyContent: "space-evenly",
        padding: "10% 0",
      }}
    >
      {isPlaying ? (
        <YouTube videoId={videoId} opts={opts} onEnd={stopVideo} />
      ) : (
        [WOOL, CAKE].map((v) => (
          <VideoTumbnail {...v} startVideo={startVideo} />
        ))
      )}
    </Wrapper>
  );
};
export default Medias;
