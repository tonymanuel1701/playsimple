import { NavLink } from "react-router-dom";
import useStyles from "../../styles/cookies-styles";
import Wrapper from "../PageUtils/Wrapper";

const Cookies = ({
  message = "Our site uses cookies to enhance your experience and measure site traffic",
  btnTitle = "GOT IT!",
  more = "Learn More",
  handler,
}) => {
  const classes = useStyles();
  return (
    <Wrapper className={classes.cookiesWrapper}>
      <div>
        <p>{message}</p>
        <NavLink to="#">{more}</NavLink>
      </div>
      <button onClick={() => handler()}>{btnTitle}</button>
    </Wrapper>
  );
};
export default Cookies;
