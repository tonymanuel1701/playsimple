import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

instance.interceptors.request.use((request, err) => {
  if (err) return err;
  return request;
});

instance.interceptors.response.use((response) => response);

export default instance;
