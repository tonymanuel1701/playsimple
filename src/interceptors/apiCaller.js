import axios from "./axios";

export const getCall = async (URL, data = "") => {
  return axios
    .get(URL, data)
    .then((response) => response.data)
    .catch((error) => console.log(error));
};
