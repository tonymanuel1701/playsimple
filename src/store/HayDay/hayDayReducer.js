import { SET_EVOLVING, SET_FARM_WITH_FAMILY } from "./actionTypes";

const initialState = {
  evolving: { title: "", description: "" },
  farmWithFamily: { title: "", description: "" },
};

const hayDayReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FARM_WITH_FAMILY:
      return {
        ...state,
        farmWithFamily: {
          title: action.title,
          description: action.description,
        },
      };
    case SET_EVOLVING:
      return {
        ...state,
        evolving: {
          title: action.title,
          description: action.description,
        },
      };
    default:
      return state;
  }
};

export default hayDayReducer;
