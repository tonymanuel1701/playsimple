import { SET_EVOLVING, SET_FARM_WITH_FAMILY } from "./actionTypes";
import { getCall } from "../../interceptors/apiCaller";

export const getEvolving = (URL = "") => {
  return (dispatch) => {
    getCall(URL)
      .then((res) => dispatch(setEvolving(res)))
      .catch((e) => console.log(e));
  };
};
export const getFarmWithFamily = (URL = "") => {
  return (dispatch) => {
    getCall(URL)
      .then((res) => dispatch(setFarmWithFamily(res)))
      .catch((e) => console.log(e));
  };
};

export const setEvolving = ({ title, description }) => {
  return { type: SET_EVOLVING, title, description };
};

export const setFarmWithFamily = ({ title, description }) => {
  return { type: SET_FARM_WITH_FAMILY, title, description };
};
