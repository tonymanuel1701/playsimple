/**
 * Add all the future reducers to the rootReducers object.
 * Root reducer is exported and used in store.js
 */

import hayDayReducer from "./HayDay/hayDayReducer";

const rootReducers = { hayDayReducer: hayDayReducer };
export default rootReducers;
