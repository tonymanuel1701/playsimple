export const PATH_HOME = "/";
export const PATH_HAY_DAY = "/games/hayday";

export const PATH_OUR_STORY = "https://supercell.com/en/our-story/";
export const PATH_GAMES = "/games";
export const PATH_CAREERS = "https://supercell.com/en/careers/";
export const PATH_SUPPORT = "https://supercell.com/en/support/";
export const PATH_SUPERCELL_ID = "https://supercell.com/en/supercell-id/";

export const PATH_SAFE_AND_FAIR_PLAY =
  "https://supercell.com/en/safe-fair-play/";

export const PATH_CLASH_OF_CLANS =
  "https://supercell.com/en/games/clashofclans/";

export const PATH_BOOM_BEACH = "https://supercell.com/en/games/boombeach/";
export const PATH_CLASH_ROYALE = "https://supercell.com/en/games/clashroyale/";
export const PATH_BRAWL_STARS = "https://supercell.com/en/games/brawlstars/";

export const PATH_TERMS_OF_SERVICE =
  "https://supercell.com/en/terms-of-service/";
export const PATH_PRIVACY_POLICY = "https://supercell.com/en/privacy-policy/";
export const PATH_PARENTS_GUIDE = "https://supercell.com/en/parents/";
export const PATH_SAFE_AND_FAIR_POLICY =
  "https://supercell.com/en/safe-and-fair-play/";

export const PATH_SOCIAL_YOUTUBE = "https://supr.cl/2FyB6q2";
export const PATH_SOCIAL_FACEBOOK = "https://supr.cl/353IhiL";
export const PATH_SOCIAL_INSTAGRAM = "https://supr.cl/3j8f2Ai";
export const PATH_SOCIAL_TWITTER = "https://supr.cl/2pzzIsf";
export const PATH_SOCIAL_LINKEDIN = "https://supr.cl/2T3yYJS";
export const PATH_SOCIAL_GLASSDOOR = "https://supr.cl/2H81F5R";

export const PATH_STORE_IOS = "https://supr.cl/appstorescgames";
export const PATH_STORE_PLAY =
  "https://play.google.com/store/apps/dev?id=6715068722362591614&hl=en";
export const PATH_STORE_AMAZON =
  "https://www.amazon.com/Supercell-ltd-Clash-of-Clans/dp/B07K7M5CMG/";

export const PATH_360_VIDEO = "eL7GZYOFrDc";
export const PATH_NEIGHBORHOOD_VIDEO = "eL7GZYOFrDc";
export const PATH_CAKE_VIDEO = "OStAo1NTi2k";

export const PATH_WOOL_VIDEO = "5BQOgUWU59w";
