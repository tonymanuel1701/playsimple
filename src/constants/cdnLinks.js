export const CDN_LOGO =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/logo_supercell@2x.png";

export const CDN_SUPERCELL_ID =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/logo_supercell_id.png";

export const CDN_SEARCH_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/search-icon.png";

export const CDN_APP_STORE_IMAGE =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/footer_appstore.png";

export const CDN_PLAY_STORE_IMAGE =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/footer_googleplay.png";

export const CDN_AMAZON_STORE_IMAGE =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/btn_amazon_appstore.png";

export const CDN_YOUTUBE_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/some_youtube.png";

export const CDN_FACEBOOK_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/some_facebook.png";

export const CDN_INSTAGRAM_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/some_instagram.png";

export const CDN_TWITTER_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/some_twitter.png";

export const CDN_LINKEDIN_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/some_linkedin.png";

export const CDN_GLASSDOOR_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/some_glassdoor.png";

export const CDN_IMAGE_HAY_DAY =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_thumbnail_hayday.jpg";

export const CDN_IMAGE_CLASH_OF_CLANS =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_thumbnail_clashofclans.jpg";

export const CDN_IMAGE_BOOM_BEACH =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_thumbnail_boombeach.jpg";

export const CDN_IMAGE_CLASH_ROYALS =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_thumbnail_clashroyale.jpg";

export const CDN_IMAGE_BRAWL_STARS =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_thumbnail_brawlstars.jpg";

export const CDN_IMAGE_GAMES_BANNER =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/styles/hero_image_large/public/bg_hero_games.jpg?itok=RD5O4aUm&timestamp=1605709726";

export const CDN_IMAGE_HAY_DAY_BANNER =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/styles/hero_image_large/public/bg_hero_hayday.jpg";

export const CDN_IMAGE_HAY_DAY_LOGO =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/main_logo_hayday.png";

export const CDN_CHEVRON_DOWN =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/bg_arrow_btn.png";

export const CDN_HAY_DAY_INTRO =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/bg_intro_hayday.jpg";

export const CDN_NEWS_INTRO =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/bg_gamewebsite_hayday.png";

export const CDN_HAY_DAY_COMMUNITY_BANNER =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/games_hayday_community_bg.jpg";

export const CDN_VIDEO_WOOL_THUMBNAIL =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_hayday_video_toomuchwool_thumbnail.jpg";

export const CDN_VIDEO_CAKE_THUMBNAIL =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_hayday_video_cake_thumbnail.jpg";
export const CDN_VIDEO_NEIGHBOUR_THUMBNAIL =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_hayday_video_grouphug_thumbnail.jpg";
export const CDN_VIDEO_360_THUMBNAIL =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/games_hayday_video_hayday360_thumbnail.jpg";

export const CDN_PLAY_ICON =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/btn_play_video.png";

export const CDN_YOUTUBE_ICON_FULL =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/bg_logo_youtube.png";

export const CDN_TIMELINE_BAR =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/bg_games_timeline_bar.png";

export const CDN_BG_TIMELINE_HAYDAY =
  "https://cdn.supercell.com/supercell.com/210319070834/all/themes/supercell/img/18/bg_timeline_hayday.png";

export const HOME_BANNER =
  "https://cdn.supercell.com/supercell.com/210319070834/supercell.com/files/styles/hero_image_normal/public/bg_hero_frontpage.jpg?itok=jbkEwRSH&timestamp=1544480104";
