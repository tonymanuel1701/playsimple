export const OUR_STORY = "Our Story";
export const GAMES = "Games";
export const CAREERS = "Careers";
export const SUPPORT = "Support";
export const SAFE_AND_FAIR_PLAY = "Safe & Fair Play";

export const HAY_DAY = "Hay Day";

export const CLASH_OF_CLANS = "Clash Of Clans";

export const BOOM_BEACH = "Boom Beach";

export const CLASH_ROYALE = "Clash Royale";

export const BRAWL_STARS = "Brawl Stars";

export const TERMS_OF_SERVICE = "Terms Of Service";
export const PRIVACY_POLICY = "Privacy Policy";
export const PARENTS_GUIDE = "Parent's Guide";
export const SAFE_AND_FAIR_POLICY = "Safe and Fair Play Policy";
