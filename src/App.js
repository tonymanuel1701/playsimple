import NavBar from "./components/navbar/NavBar";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Footer from "./components/footer/Footer";
import Games from "./pages/Games";
import { PATH_GAMES, PATH_HAY_DAY, PATH_HOME } from "./constants/routes";
import Wrapper from "./components/PageUtils/Wrapper";
import HayDay from "./pages/HayDay";
import Cookies from "./components/cookies/Cookies";
import { useCookies } from "react-cookie";
import { useEffect, useState } from "react";
import ScrollToTop from "./ScrollToTop";
import Home from "./pages/Home";

function App({ cookieName = "playSimple", cookieExpiry = 2147483647 }) {
  const [cookies, setCookie] = useCookies(["name"]);
  const [showCookie, setShowCookie] = useState(false);

  const acceptCookiesHandler = (consent = "Cookies Ok") => {
    setCookie(cookieName, consent, { path: "/", maxAge: cookieExpiry });
    setShowCookie(false);
  };

  const checkCookies = () => {
    if (!cookies[cookieName]) {
      setShowCookie(true);
    }
  };

  useEffect(() => {
    checkCookies();
  }, []);

  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <Wrapper style={{ minHeight: 200 }}>
          <ScrollToTop>
            <Switch>
              <Route path={PATH_HAY_DAY} component={HayDay} />
              <Route path={PATH_GAMES} component={Games} />
              <Route path={PATH_HOME} component={Home} />
            </Switch>
          </ScrollToTop>
        </Wrapper>
        <Footer />
        {showCookie && <Cookies handler={acceptCookiesHandler} />}
      </BrowserRouter>
    </div>
  );
}

export default App;
