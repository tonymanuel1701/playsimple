import * as MENU from "../constants/menus";
import * as ROUTE from "../constants/routes";
const SECONDARY_MENUS = [
  {
    title: MENU.TERMS_OF_SERVICE,
    route: ROUTE.PATH_TERMS_OF_SERVICE,
    target: "_blank",
  },
  {
    title: MENU.PRIVACY_POLICY,
    route: ROUTE.PATH_PRIVACY_POLICY,
    target: "_blank",
  },
  {
    title: MENU.PARENTS_GUIDE,
    route: ROUTE.PATH_PARENTS_GUIDE,
    target: "_blank",
  },
  {
    title: MENU.SAFE_AND_FAIR_POLICY,
    route: ROUTE.PATH_SAFE_AND_FAIR_POLICY,
    target: "_blank",
  },
];
export default SECONDARY_MENUS;
