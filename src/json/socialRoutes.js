import * as ICONS from "../constants/cdnLinks";
import * as ROUTES from "../constants/routes";

export const YOUTUBE = {
  icon: ICONS.CDN_YOUTUBE_ICON,
  path: ROUTES.PATH_SOCIAL_YOUTUBE,
};
export const FACEBOOK = {
  icon: ICONS.CDN_FACEBOOK_ICON,
  path: ROUTES.PATH_SOCIAL_FACEBOOK,
};
export const INSTAGRAM = {
  icon: ICONS.CDN_INSTAGRAM_ICON,
  path: ROUTES.PATH_SOCIAL_INSTAGRAM,
};
export const TWITTER = {
  icon: ICONS.CDN_TWITTER_ICON,
  path: ROUTES.PATH_SOCIAL_TWITTER,
};
export const LINKEDIN = {
  icon: ICONS.CDN_LINKEDIN_ICON,
  path: ROUTES.PATH_SOCIAL_LINKEDIN,
};
export const GLASSDOOR = {
  icon: ICONS.CDN_GLASSDOOR_ICON,
  path: ROUTES.PATH_SOCIAL_GLASSDOOR,
};
