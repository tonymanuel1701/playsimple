import {
  CDN_VIDEO_360_THUMBNAIL,
  CDN_VIDEO_CAKE_THUMBNAIL,
  CDN_VIDEO_NEIGHBOUR_THUMBNAIL,
  CDN_VIDEO_WOOL_THUMBNAIL,
} from "../constants/cdnLinks";
import * as ROUTES from "../constants/routes";

export const WOOL = {
  title: "Too Much Wool",
  path: ROUTES.PATH_WOOL_VIDEO,
  thumbnail: CDN_VIDEO_WOOL_THUMBNAIL,
};
export const CAKE = {
  title: "Cake",
  path: ROUTES.PATH_CAKE_VIDEO,
  thumbnail: CDN_VIDEO_CAKE_THUMBNAIL,
};
export const NEIGHBORHOOD = {
  title: "Neighborhood Group Hug",
  path: ROUTES.PATH_NEIGHBORHOOD_VIDEO,
  thumbnail: CDN_VIDEO_NEIGHBOUR_THUMBNAIL,
};
export const HUG360 = {
  title: "360 Hug",
  path: ROUTES.PATH_360_VIDEO,
  thumbnail: CDN_VIDEO_360_THUMBNAIL,
};
