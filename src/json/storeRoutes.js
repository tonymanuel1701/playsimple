import * as ICONS from "../constants/cdnLinks";
import * as ROUTES from "../constants/routes";

export const AMAZON = {
  icon: ICONS.CDN_AMAZON_STORE_IMAGE,
  path: ROUTES.PATH_STORE_AMAZON,
};

export const IOS = {
  icon: ICONS.CDN_APP_STORE_IMAGE,
  path: ROUTES.PATH_STORE_IOS,
};
export const GOOGLE = {
  icon: ICONS.CDN_PLAY_STORE_IMAGE,
  path: ROUTES.PATH_STORE_PLAY,
};
