import * as MENU from "../constants/menus";
import * as ROUTE from "../constants/routes";
import Games from "../pages/Games";

const NAV_LINKS = [
  {
    title: MENU.OUR_STORY,
    route: ROUTE.PATH_OUR_STORY,
    target: "_blank",
  },
  {
    title: MENU.GAMES,
    route: ROUTE.PATH_GAMES,

    subMenu: [
      {
        title: MENU.HAY_DAY,
        route: ROUTE.PATH_HAY_DAY,
        target: "",
      },
      {
        title: MENU.CLASH_OF_CLANS,
        route: ROUTE.PATH_CLASH_OF_CLANS,
        target: "_blank",
      },
      {
        title: MENU.BOOM_BEACH,
        route: ROUTE.PATH_BOOM_BEACH,
        target: "_blank",
      },
      {
        title: MENU.CLASH_ROYALE,
        route: ROUTE.PATH_CLASH_ROYALE,
        target: "_blank",
      },
      {
        title: MENU.BRAWL_STARS,
        route: ROUTE.PATH_BRAWL_STARS,
        target: "_blank",
      },
    ],
  },
  {
    title: MENU.CAREERS,
    route: ROUTE.PATH_CAREERS,
    target: "_blank",
  },
  {
    title: MENU.SUPPORT,
    route: ROUTE.PATH_SUPPORT,
    target: "_blank",
  },
  {
    title: MENU.SAFE_AND_FAIR_PLAY,
    route: ROUTE.PATH_SAFE_AND_FAIR_PLAY,
    target: "_blank",
  },
];
export default NAV_LINKS;
