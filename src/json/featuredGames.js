import * as ROUTE from "../constants/routes";
import * as TITLE from "../constants/menus";
import {
  CDN_IMAGE_BOOM_BEACH,
  CDN_IMAGE_BRAWL_STARS,
  CDN_IMAGE_CLASH_OF_CLANS,
  CDN_IMAGE_CLASH_ROYALS,
  CDN_IMAGE_HAY_DAY,
} from "../constants/cdnLinks";

const FEATURED_GAMES = [
  {
    title: TITLE.HAY_DAY,
    subTitle: "FARM WITH FRIENDS AND FAMILY",
    route: ROUTE.PATH_HAY_DAY,
    imageSrc: CDN_IMAGE_HAY_DAY,
    target: "",
  },
  {
    title: TITLE.CLASH_OF_CLANS,
    subTitle: "LEAD YOUR CLAN TO VICTORY!",
    route: ROUTE.PATH_CLASH_OF_CLANS,
    imageSrc: CDN_IMAGE_CLASH_OF_CLANS,
    target: "_blank",
  },
  {
    title: TITLE.BOOM_BEACH,
    subTitle: "BUILD. PLAN. BOOM!",
    route: ROUTE.PATH_BOOM_BEACH,
    imageSrc: CDN_IMAGE_BOOM_BEACH,
    target: "_blank",
  },
  {
    title: TITLE.CLASH_ROYALE,
    subTitle: "EPIC REAL-TIME CARD BATTLES",
    route: ROUTE.PATH_CLASH_ROYALE,
    imageSrc: CDN_IMAGE_CLASH_ROYALS,
    target: "_blank",
  },
  {
    title: TITLE.BRAWL_STARS,
    subTitle: "3V3 AND BATTLE ROYALE",
    route: ROUTE.PATH_BRAWL_STARS,
    imageSrc: CDN_IMAGE_BRAWL_STARS,
    target: "_blank",
  },
];
export default FEATURED_GAMES;
