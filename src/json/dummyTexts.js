export const FARM_WITH_FRIENDS = {
  title: "FARM WITH FRIENDS AND FAMILY",
  body: ` You don’t have to be from a farm to be a great farmer. 
            Hay Day lets you get back to nature and experience the simple life of working the land.
            It’s a special game set in a real special place. Food grows free, people smile and the 
            animals are always happy to see you. It never rains here, but the crops never die. 
            And if you’re craving a little bacon, one of the pigs will be happy to fry some up for you.
            After all, what are pigs... oops, we mean neighbors for?`,
};
export const HAYDAY_COMMUNITY = {
  title: "STAY UP-TO-DATE",
  body: `   To stay on top of your game, keep an eye on the in-game News section. 
            Follow us on social media for the latest chatter and sneak peeks on what
            the team is working on. Don’t be a stranger and join the conversation.`,
};

export const EVOLVING = {
  title: "CONSTANTLY EVOLVING",
  body: `  Hay Day has been constantly evolving to offer more user-friendly,
            consistent and fun online experiences for Supercell gamers. There
            have been countless updates since the game launched in 2012.`,
};
