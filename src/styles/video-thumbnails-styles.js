import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  thumbnailWrapper: {
    cursor: "pointer",
    height: "50%",
    width: "40%",
    "&:hover": {
      color: "#777777",
    },
  },

  thumbnailFooter: {
    background: "#fff",
    height: "100px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontSize: "18px",
    color: "#2d85f3",
    padding: "20px",
    boxSizing: "border-box",
    "& div": { width: "100px" },
  },

  thumbnailVideo: { height: "300px" },
  thumbnailPlay: { width: "25% !important", margin: "auto" },
});
export default useStyles;
