import { createUseStyles } from "react-jss";
import { CDN_SEARCH_ICON } from "../constants/cdnLinks";
const useStyles = createUseStyles({
  wrapper: {
    position: "fixed",

    zIndex: 9999,
    margin: "0 auto",
    width: "100%",
    height: "78px",
    display: "flex",
    flexFlow: "row",
    justifyContent: "space-between",
    alignItems: "center",
    background: "#000",
    padding: "0 15px",
    boxSizing: "border-box",
  },

  innerWrapper: {
    display: "flex",
    justifyContent: "space-between",
    flexGrow: 1,
    alignItems: "center",
    height: "100%",
  },

  link: {
    fontSize: "15px",
    textDecoration: "none",
    margin: "0 19.5px",
    color: "#808080",
    transition: "0.2s",
    "&:hover": {
      color: "#fff",
      transition: "0.2s",
    },
  },
  subMenu: {
    zIndex: 9998,
    display: "flex",
    alignItems: "center",
    height: "78px",
    width: "1200px",
    background: "rgba(0,0,0,0.75)",
  },

  superCellWrapper: {
    height: "100%",
    borderRight: "1px solid #363636",
    transition: "all 0.5s ease-out",
  },

  superCell: {
    display: "block",
    marginRight: "32px",
    height: "100%",
    width: "120px",
  },

  searchIcon: {
    height: "20px",
    width: "20px",
    display: "block",
    opacity: "0.3",
    transition: "0.2s",
    cursor: "pointer",
    "&:hover": {
      opacity: 1,
      transition: "0.2s",
    },

    backgroundImage: `url(${CDN_SEARCH_ICON})`,
  },
  searchInput: {
    transition: "all 0.3s ease-out",

    "& input": {
      position: "absolute",
      left: "220px",
      background: "transparent",
      border: 0,
      outline: 0,
      color: "#c0c0c0",
      fontSize: "16px",
      borderBottom: "1px solid #404040",
    },
  },

  searchResultsWrapper: {
    position: "fixed",
    top: "75px",
    zIndex: "1",
    left: 0,
    right: 0,
    color: "#fff",
    minHeight: "200px",
    background: "#1c1c1c",
    padding: "50px",
  },
  searchResultsContent: {
    display: "flex",
    alignItems: "center",
    borderBottom: "1px solid #404040",
    transition: "all 0.3s ease",
    "& h3,p": { transition: "all 0.3s ease" },
    "& h3": { width: "20%", fontSize: "15px", color: "#939393" },
    "& p": { fontSize: "14px", color: "#575757" },

    "&:hover": {
      "& h3,p": {
        color: "#fff !important",
        transition: "all 0.3s ease",
      },
    },
  },
});

export default useStyles;
