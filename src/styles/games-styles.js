import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  gridWrapper: {
    display: "grid",
    gridTemplateColumns: "auto auto",
    gridGap: "60px 60px",
    padding: "50px",
  },

  featureWrapper: {
    textAlign: "center",
  },
  title: {
    textDecoration: "none",
    fontSize: "20px",
    color: "#000",
    "&:hover": {
      color: "#555555",
    },
  },
  subTitle: {
    fontSize: "14px",
    fontWeight: "bold",
    color: "#fb4a9d",
    textTransform: "uppercase",
  },
});

export default useStyles;
