import { createUseStyles } from "react-jss";
const useStyles = createUseStyles({
  cookiesWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "1em 1.8em",
    backgroundColor: "rgba(0, 0, 0, 0.9)",
    color: "#AAAAAA",
    position: "fixed",
    bottom: 0,
    width: "100%",
    height: "70px",
    boxSizing: "border-box",
    transition: "opacity 0.3s",

    "& p": { fontSize: "14px", marginBottom: 0 },
    "& a": {
      fontSize: "14px",
      fontWeight: "bold",
      color: "#fff",
      textDecoration: "none",
    },
    "& button": {
      padding: "10px 25px",
      fontWeight: "bold",
      background: "#008bf7",
      border: "none",
      outline: 0,
      cursor: "pointer",
      borderRadius: 5,
      color: "#fff",
      transition: "all 0.3s",
      "&:hover": {
        background: "#36a0f2",
        transition: "all 0.3s",
      },
    },
  },
});
export default useStyles;
