import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  wrapper: {
    position: "relative",
    height: "680px",
    "& img": {
      width: "100%",
      height: "100%",
      objectFit: "cover",
      position: "absolute",
    },
  },
});

export default useStyles;
