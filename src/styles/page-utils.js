import { createUseStyles } from "react-jss";
import { CDN_TIMELINE_BAR } from "../constants/cdnLinks";

const useStyles = createUseStyles({
  gridContainer: {
    display: "grid",
    gridTemplateColumns: "50% 50%",
    alignItems: "center",
  },
  addressWrapper: {
    margin: "40px 0",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  address: {
    display: "flex",
    flexFlow: "column",
    color: "#808080",
    "& span": {
      display: "block",
      marginTop: "8px",
    },
  },

  bgImage: {
    height: "100%",
    width: "100%",
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    backgroundPosition: "center",
  },

  brand: {
    height: "54px",
    width: "54px",
  },

  socialIconsWrapper: {
    display: "grid",
    gridTemplateColumns: "repeat(12, 1fr)",
    marginTop: "18px",
  },

  socialIcons: { height: "32px", width: "27px" },

  storeLinksWrapper: { display: "flex", justifyContent: "flex-end" },

  storeLinks: {
    marginRight: "10px",
    height: "50px",
    width: "150px",
    backgroundSize: "100% 100%",
  },

  dividerH: {
    borderTop: "1px solid #333",
    margin: "40px 0",
  },

  footer: {
    height: "auto",
    color: "#fff",
    width: "100%",
    background: "#000",
    padding: "33px 75px",
    boxSizing: "border-box",
  },

  mediumText: { margin: 0, fontSize: "13px", fontWeight: "normal" },

  secondaryMenus: {
    "& a": {
      marginRight: "20px",
      textDecoration: "none",
      color: "#fff",
    },
  },

  bannerTitle: {
    position: "absolute",
    width: "35%",
    left: "0",
    right: "0",
    margin: "0 auto",
    textAlign: "center",
    top: "55%",
    "& h2": {
      margin: 0,
      fontSize: "40px",
      textTransform: "uppercase",
    },
    "& p": { fontSize: "20px" },
  },

  bannerAction: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: "-30px",
    marginLeft: "auto",
    marginRight: "auto",
    width: "180px",

    display: "inline-block",
    background: "#2979de",
    color: "#fff",
    borderRadius: 50,
  },
  chevron: {
    padding: "15px",
    background: "#1c70d8",
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    fontSize: "13px",
    "& img": { width: "15px", height: "auto" },
  },
  actionLabel: {
    fontSize: "13px",
    display: "inline-block",
    color: "#fff",
    padding: "15px 20px",
    fontWeight: "bold",
  },

  panelTitle: {
    textTransform: "uppercase",
    fontSize: "34px",
    marginBottom: "8.5px",
  },

  panelBody: {
    fontSize: "18px",
    paddingBottom: "22.5px",
    lineHeight: "1.5",
  },

  panelIntroImage: {
    height: "480px",
    float: "left",
    left: "-70%",
    position: "relative",
  },

  scrollContainer: {
    content: "",
    display: "inline-block",
    height: "32px",
    position: "absolute",
    top: "137px",
    left: 0,
    right: 0,
    background: `url(${CDN_TIMELINE_BAR})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "100% auto",
    borderTopLeftRadius: "4px",
    borderBottomLeftRadius: "4px",
  },

  mileStones: {
    float: "left",
    borderLeft: "1px solid #ddd",
    borderRight: "1px solid #ddd",
    padding: "0 20px 0 10px",
    width: "120px",
    height: "73px",
    boxSizing: "border-box",
    fontSize: "13px",
    fontWeight: "bold",
  },
  hayDay: {
    position: "absolute",
    zIndex: 1,
    right: 0,
    height: "328px",
    width: "237px",
    top: "-130%",
    "& img": {
      height: "100%",
    },
  },
});

export default useStyles;
